#encoding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Category.create name: "Computación"
Category.create name: "Arquitectura"
Category.create name: "Diseño"
Category.create name: "Leyes"
Category.create name: "Administración"
Category.create name: "Preparatoria"