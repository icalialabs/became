class CreateAngels < ActiveRecord::Migration
  def change
    create_table :angels do |t|
      t.string :name

      t.timestamps
    end
  end
end
