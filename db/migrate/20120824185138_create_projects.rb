class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :title
      t.string :goal
      t.date :finish_date
      t.integer :angels_count
      t.integer :funded_count
      t.integer :student_id

      t.timestamps
    end
     add_index :projects, :student_id
  end
end
