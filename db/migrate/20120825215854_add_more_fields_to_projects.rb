class AddMoreFieldsToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :why, :text
    add_column :projects, :dreams, :text
    add_column :projects, :abilities, :text
    add_column :projects, :better_country, :text
  end
end
