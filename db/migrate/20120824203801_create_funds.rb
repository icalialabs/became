class CreateFunds < ActiveRecord::Migration
  def change
    create_table :funds do |t|
      t.integer :angel_id
      t.integer :project_id
      t.decimal :amount

      t.timestamps
    end
    add_index :funds, :angel_id
    add_index :funds, :project_id
  end
end
