class ChangeProjectGoalToInt < ActiveRecord::Migration
  def up
    change_column :projects, :goal, :decimal 
    change_column :projects, :funded_count, :decimal, default: 0
    change_column :projects, :angels_count, :integer, default: 0
  end

  def down
    change_column :projects, :goal, :string
    change_column :projects, :funded_count, :integer
  end
end
