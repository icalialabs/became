# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
numberWithCommas = (x) -> 
    x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")

$ ->
    new Highcharts.Chart({
        chart: {
            renderTo: 'pronabes-chart'
            zoomType: 'xy'
        }
        title: {
            text: 'Inversión e Ingreso Federal en PRONABES'
        }
        xAxis: [{
            categories: ['AGS','BCN','BCS','CAM','CHH',
            'CHP','COA','COL','DUR',        
            'GRO','GUA','HID','JAL','MEX',        
            'MIC','MOR','NAY','NL','OAX',
            'PUE','QUE','ROO','SIN','SLP',
            'SON','TAB','TAM','TLA','VER',
            'YUC','ZAC'
            ]
        }]
        yAxis: [{
            labels: {
                style: {
                    color: '#89A54E'
                }
            }
            title: {
                text: 'Ingresos Federales'
                style: {
                    color: '#89A54E'
                }
            }
        }, {
            title: {
                text: 'Dinero Invertido en Becas'
                style: {
                    color: '#4572A7'
                }
            }
            labels: {
                style: {
                    color: '#4572A7'
                }
            }
            opposite: true
        }]
        tooltip: {
            formatter: ->
                this.x + ': $ ' + numberWithCommas(this.y)
        }
        legend: {
            layout: 'vertical'
            align: 'center'
            x: 120
            verticalAlign: 'top'
            y: 100
            floating: true
            backgroundColor: '#FFFFFF'
        }
        credits: {
            enabled: false
        }
        series: [{
            name: 'Dinero Invertido en becas'
            color: '#4572A7'
            type: 'bar'
            yAxis: 1
            data: [0, 159878960, 14087960, 0, 52295980, 90183960, 52295980, 18546662, 0, 0, 20862569, 47214582, 104436500, 15038061, 72382241, 0, 33923340, 36394170, 59908240, 150922240, 51686850, 37794127, 25016188, 0, 0, 63087880, 0, 24224140, 343203290, 0, 39134330]
        }, {
            name: 'Ingresos Federales'
            color: '#89A54E'
            type: 'bar'
            data: [11500000, 13528000, 14000000, 15000000, 50200000, 63629000, 25000000, 9000000, 22000000, 125000000, 73000000, 22450000, 51000000, 55000000, 46400000, 7050000, 20000000, 17000000, 30000000, 80000000, 24000000, 20000000, 23000000, 25900000, 70000000, 47100000, 66939000, 15000000, 130000000, 50108000, 45000000]
        }]
    }, ->
        Elastic.refresh()
    )
