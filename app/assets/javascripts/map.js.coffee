numberWithCommas = (x) -> 
    x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")

colorOff = '#666'
edo_init = 'OAX'
#Goals the edos
edos = ['AGU','BCN','BCS','CAM',
		'CHP','CHH','COA','COL','DIF','DUR','MEX',
		'GUA','GRO','HID','JAL',
		'MIC','MOR','NAY','NLE','OAX',
		'PUE','QUE','ROO','SLP','SIN',
		'SON','TAB','TAM','TLA','VER',
		'YUC','ZAC'
		]
edos_names = [	'Aguascalientes','Baja California','Baja California Sur','Campeche',
				'Chiapas','Chihuahua','Coahuila','Colima','Distrito Federal','Durango', 'Edo. México'
				'Guanajuato','Guerrero','Hidalgo','Jalisco',
				'Michoacán','Morelos','Nayarit','Nuevo León','Oaxaca',
				'Puebla','Querétaro','Quintana Roo','San Luis Potosí','Sinaloa',
				'Sonora','Tabasco','Tamaulipas','Tlaxcala','Veracruz',
				'Yucatán','Zacatecas'
			]
			
anos = {
        2006: [1569, 2158, 1548, 1374, 8037, 5841, 5070, 1217, "Sin datos", 3966, 17519, 6129, 4088, 6041, 7320, 6919, 1346, 2144, 4495, 2251, 7482, 1727, 2287, 4481, 3671, 3475, 8512, 7128, 1771, 21267, 4264, 4273],
        2008: [2723, 3419, 2798, 2622, 6809, 7802, 5293, 1827, "Sin datos", 4534, 26214, 10322, 4852, 10592, 9583, 9924, 1703, 2278, 3954, 5878, 15180, 4315, 4213, 4397, 4845, 5572, 9686, 11741, 2440, 28090, 7656, 6046],
        2010: [2236, 2500, 2829, 2986, 11099, 9144, 5290, 1857, "Sin datos", 3943, 23141, 11650, 4475, 12270, 11945, 8938, 1500, 3042, 4187, 6967, 14089, 4673, 3317, 4217, 5916, 10565, 6336, 12817, 2063, 24249, 9565, 7402]
      }
			
window.max = 0
window.min = Infinity

actual_year = 2010

$.each anos[actual_year], (i, e) ->
  n = parseInt(e)

  if isNaN(n)
    return

  if window.max < n
    window.max = n
  if window.min > n
    window.min = n


fillColor = (edo, n) ->

  startColor = "FFFFFF"
  endColor = "000000"

  color = interpolateColor(startColor, endColor, max-min, n-min)
  console.log edo.attr("id"), color

  if edo[0].nodeName != 'g'
    edo.css({fill: color})
  else
    paths = edo.children("path")
    $.each paths, (i, e) ->
      $(e).css({fill: color})


init_map = ->
  svg = $("#mx_map").svg('get')
  $.each edos, (i, e) ->
    edo = $("##{e}")
    edo.css({cursor: "pointer"})
    fillColor(edo, anos[actual_year][i])

    edo.bind "mouseover", () ->
      $("#edo").html("<b>" + edos_names[i] + "</b><br /> Beneficiarios: " + numberWithCommas(anos[actual_year][i]))

    $("h2 .year").html(actual_year)

jQuery ->
  $("#mx_map").svg {
    loadURL: "/assets/mx_map.svg"
    onLoad: (svg) ->
      $("#mx_map svg").attr("height", "450px")
      init_map()

  }

  $("input[name=year]").bind "change", (e) ->
    actual_year = $("[name=year]:checked").val()
    init_map()
