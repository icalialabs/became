# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
jQuery ->
	faye = new Faye.Client('http://gentle-waters-9528.herokuapp.com/faye')
	faye.disable('websocket')
	faye.subscribe "/projects/new", (data) -> 
		$('#count_up').text(data.projects_count)