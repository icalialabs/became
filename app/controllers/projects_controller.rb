class ProjectsController < ApplicationController
  
  def index
    @projects = Project.includes(:student)
  end

  def new
    @student = current_user.userable
    @project = @student.projects.build
  end
  
  def create
    @student = current_user.userable
    @project = @student.projects.build(params[:project])
    if @project.save
      broadcast "/projects/new", {projects_count: Project.count}
      redirect_to @project
    else
      render :new
    end
  end

  def show
    @project = Project.find(params[:id])
    @fund = @project.funds.build
  end

  def edit
  end
end
