class ApplicationController < ActionController::Base
  protect_from_forgery
  
  def broadcast(channel, hash)
    message = {:channel => channel, :data => hash}
    uri = URI.parse("http://gentle-waters-9528.herokuapp.com/faye")
    Net::HTTP.post_form(uri, :message => message.to_json)
  end
end
