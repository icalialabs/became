class AngelsController < ApplicationController
  def new
    @angel = Angel.new
  end

  def create
    @angel = Angel.new(params[:angel])
    if @angel.save
      sign_in_and_redirect @angel.user
    else
      render :new
    end
  end
end
