class FundsController < ApplicationController
  
  def create
    @project = Project.find(params[:project_id])
    @fund = @project.funds.build(params[:fund])
    @fund.angel = current_user.userable
    if @fund.save
      redirect_to @fund.project, flash: {success: "Wohooo"}
    else
      redirect_to @fund.project, flash: {error: "Buuuuhh"}
    end
  end
end
