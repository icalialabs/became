class Fund < ActiveRecord::Base
  attr_accessible :amount
  
  belongs_to :project, counter_cache: "angels_count"
  belongs_to :angel

  before_save :update_funded_counter
  before_destroy :update_funded_counter

  def update_funded_counter
    self.project.update_attribute :funded_count, self.project.funds.sum(:amount)+self.amount
  end


end
