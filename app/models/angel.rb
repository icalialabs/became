class Angel < ActiveRecord::Base
  acts_as_user
  attr_accessible :name, :user_attributes
  
  has_many :funds, dependent: :destroy
  has_many :projects, through: :funds
  
  accepts_nested_attributes_for :user
end
