#encoding: utf-8
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me,
                  :state, :city, :bio
  # attr_accessible :title, :body
  
  belongs_to :userable, polymorphic: true
  
  STATES = ["Aguascalientes", "Baja California", "Baja California Sur", 
             "Campeche", "Chiapas", "Chihuahua", "Coahuila", "Colima", 
             "Distrito", "Federal", "Durango", "Estado de México", "Guanajuato", 
             "Guerrero", "Hidalgo", "Jalisco", "Michoacán", "Morelos", "Nayarit", 
             "Nuevo León", "Oaxaca", "Puebla", "Querétaro", "Quintana Roo", 
             "San Luis Potosí", "Sinaloa", "Sonora", "Tabasco", "Tamaulipas", 
             "Tlaxcala", "Veracruz", "Yucatán", "Zacatecas"]
  
  
  def self.from_omniauth(auth)
    where(auth.slice(:provider, :uid)).first_or_create do |user|
        first_name = auth["extra"]["raw_info"].try("[]", "first_name")
        birthday = Date.strptime auth["extra"]["raw_info"]["birthday"], "%m/%d/%Y" if auth["extra"].try("[]", "raw_info").try("[]", "birthday")
        gender = auth["extra"].try("[]", "raw_info").try("[]", "gender")
        student = Student.new(name: first_name, birthdate: birthday, gender: gender)
        student.save(validate: false) if user.new_record?
        user.provider = auth.provider
        user.uid = auth.uid
        user.email = auth["info"].try("[]", "email")
        user.userable_type = "Student"
        user.userable_id = student.id if user.new_record?
        user.city = auth["info"]["location"].split(",")[0] unless auth["info"]["location"].nil?
        user.state = auth["info"]["location"].split(",")[1].strip unless auth["info"]["location"].nil?
    end
  end
  
  def password_required?
    super && provider.blank?
  end

  def update_with_password(params, *options)
    if encrypted_password.blank?
      update_attributes(params, *options)
    else
      super
    end
  end
  
  def is_student?
    self.userable_type ==  "Student"
  end
  
  def is_angel?
    self.userable_type == "Angel"
  end
  
  
  
end
