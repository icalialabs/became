class Project < ActiveRecord::Base
  attr_accessible :finish_date, :goal, :title, :category_id, :why, :dreams, :abilities, :better_country

  belongs_to :student
  
  has_many :funds, dependent: :destroy
  has_many :angels, through: :funds
  
  belongs_to :category
  validates_numericality_of :goal

  default_scope where("finish_date >= ?", Date.today)

  def days_left
    if self.finish_date
      (self.finish_date - Date.today).to_i
    else
      0
    end
  end

  def current_percentage
    (self.funded_count * 100 / self.goal).to_i
  end

end
