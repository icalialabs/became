class Student < ActiveRecord::Base
  acts_as_user
  attr_accessible :birthdate, :grade, :name, :surname, :user_attributes, :gender
  
  accepts_nested_attributes_for :user
  
  has_many :projects, dependent: :destroy
  
  def image
    "http://graph.facebook.com/#{self.uid}/picture?type=large"
  end

  def full_name
    "#{self.name} #{self.surname}"    
  end
end
