Became::Application.routes.draw do

  get "funds/create"

  root to: 'pages#home'

  match "/info" => "pages#info"
  
  resources :students, only: [:new, :create, :show, :edit, :update] do
    resources :projects, only: [:new, :edit, :create, :update]
  end
  
  resources :projects, only: [:show, :index] do
    resources :funds, only: [:create]
  end
  
  resources :angels, only: [:new, :create, :show, :edit, :update]
  
  devise_for :users, :controllers => { :omniauth_callbacks => 'omniauth_callbacks' }
  
end
